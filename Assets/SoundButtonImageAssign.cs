﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class SoundButtonImageAssign : MonoBehaviour {
    [SerializeField] List<Image> images;


    private void CheckInput()
    {
        Assert.AreEqual(images.Count, SoundManager.instance.sprites.Count, "Images and sprites should be the same length");
    }

    void Start () {
        CheckInput();

        for (int i = 0; i < SoundManager.instance.sprites.Count; i++)
        {
            images[i].sprite = SoundManager.instance.sprites[i];
        }
	}

    void Update () {
		
	}
}
