﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSprite : MonoBehaviour {
    [SerializeField] Sprite newSprite;
	
	// Update is called once per frame
	public void changeSprite() {
        GetComponent<Image>().sprite = newSprite;
	}
}
