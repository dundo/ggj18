﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour
{
    public static Action<Vector3, Vector2> OnVelocityChange;
    public static Player Instance;

    public float maxJumpHeight = 4;
	public float minJumpheight = 1;
	public float timeToJumpApex = .4f;
	public float accelerationTimeAirborne = .2f;
	public float accelerationTimeGrounded = .1f;
	float moveSpeed = 10;
    public float jumpDistance;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;

	private Controller2D controller;

	Vector2 directionalInput;
	int wallDirX;
	[HideInInspector]
	public Vector3 startPosition;
    public bool still;
	void Start () 
	{
        Instance = this;
        controller = GetComponent<Controller2D> ();

		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpheight);
		print ("Gravity: " + gravity + "  Jump Velocity: " + maxJumpVelocity);

		startPosition = transform.position;
	}

	void Update () 
	{
		CalculateVelocity ();
        if (!still)
		    controller.Move (velocity * Time.deltaTime, directionalInput);
        if(OnVelocityChange != null) OnVelocityChange(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below) 
		{
			if (controller.collisions.slidingDownMaxSlope) 
			{
				velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
			} 
			else 
			{
				velocity.y = 0;
			}
		}
	}

	public void SetDirectionalInput (Vector2 input)
	{
		directionalInput = input;
	}

	public void OnJumpInputDown ()
	{
		if (controller.collisions.below) 
		{
			if (controller.collisions.slidingDownMaxSlope) 
			{
				if (directionalInput.x != -Mathf.Sign (controller.collisions.slopeNormal.x)) // not jumping against max slope
				{
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
			} 
			else 
			{
				velocity.y = maxJumpVelocity;
                velocity.x = (maxJumpVelocity + jumpDistance) * controller.collisions.faceDir;

            }
		}
	}

	public void OnJumpInputUp ()
	{
		if (velocity.y > minJumpVelocity) 
		{
			velocity.y = minJumpVelocity;
            velocity.x = (maxJumpVelocity + jumpDistance) * controller.collisions.faceDir;
        }
	}

	void CalculateVelocity ()
	{
		float targetVelocityX = directionalInput.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
	}
}
