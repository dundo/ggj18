﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionOnHit : MonoBehaviour
{
    public float BoxX = 1;
    public float BoxY = 1;
    [SerializeField] UnityEvent action;
    [SerializeField] Color GizmoColor = Color.black;
    [SerializeField] KeyCode neededButton;

    float BoxXL;
    float BoxYU;
    float BoxXR;
    float BoxYD;

    void Start()
    {
        BoxXL = transform.position.x - (BoxX / 2);
        BoxYU = transform.position.y + (BoxY / 2);
        BoxXR = transform.position.x + (BoxX / 2);
        BoxYD = transform.position.y - (BoxY / 2);
    }

    void FixedUpdate()
    {
        CheckCollision();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = GizmoColor;
        Gizmos.DrawCube(transform.position, new Vector2(BoxX, BoxY));
    }

    void CheckCollision()
    {
        if (Controller2D.Instance.transform.position.y <= BoxYU &&
            Controller2D.Instance.transform.position.x >= BoxXL &&
            Controller2D.Instance.transform.position.y >= BoxYD &&
            Controller2D.Instance.transform.position.x <= BoxXR)
        {
            Debug.Log("Trigger");
            if (Input.GetKeyDown(neededButton) || neededButton == KeyCode.None)
            {
                if (action != null) action.Invoke();
                Debug.Log("PRessed button " + neededButton);
            }
        }
    }
}
