﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
    public static PlayerLife Instance;

    public float playerHP;
    float startPlayerHP;
    public Text lifeCounter;

    void Start()
    {
        Instance = this;
        startPlayerHP = playerHP;
        lifeCounter.text = playerHP.ToString("∞");
    }

    public void LoseALife()
    {
        Debug.Log("LoaseALife");
        if (playerHP >= 1)
        {
            playerHP = playerHP - 1;

        }
        else if (playerHP < 1)
        {
            playerHP = startPlayerHP;
        }
        lifeCounter.text = playerHP.ToString();
    }
}


