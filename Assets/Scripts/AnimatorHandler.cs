﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHandler : MonoBehaviour {
    [SerializeField] GameObject player;
    private Animator animator;
    private SpriteRenderer spriteRend;
	// Use this for initialization
	void Start () {
        animator = player.GetComponent<Animator>();
        spriteRend = player.GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        Player.OnVelocityChange += OnVelocityChange;
    }
    private void OnDisable()
    {
        Player.OnVelocityChange -= OnVelocityChange;
    }

    private void OnVelocityChange(Vector3 velocity, Vector2 direction)
    {
        animator.SetFloat("hspeed", Math.Abs(velocity.x));
        animator.SetFloat("vspeed", velocity.y);

        if (direction.x > 0) spriteRend.flipX = false;
        else if (direction.x < 0) spriteRend.flipX = true;
    }

   
}
