﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMusicButton : MonoBehaviour
{
    public static ChangeMusicButton Instance;

    private int passato = -1;
    private int presente;
    public List<AudioSource> musics;

    public AudioSource WinSound;
    public AudioSource DeadSound;

    private int x = 0;
    public Slider slide;

    void Start()
    {
        Instance = this;
        for (int i = 0; i < musics.Count; i++)
        {
            musics[i].Stop();
        }
        presente = Random.Range(0, musics.Count);
        passato = presente;
        musics[presente].Play();
        StartCoroutine("Example");
    }

    public void PlayRandomSong()
    {
        
        if (x == 0)
        {
            x = 1;
            for (int i = 0; i < musics.Count; i++)
            {
                musics[i].Stop();
            }
            do
            {
                presente = Random.Range(0, musics.Count);
            } while (presente == passato);
            passato = presente;
            musics[presente].Play();
            StartCoroutine("Example");
        }
    }

    public void PlayWinSound()
    {
        musics[presente].Stop();
        WinSound.Play();
    }


    public void PlayDeadSound()
    {
        DeadSound.Play();
    }


    IEnumerator Example()
    {
        yield return new WaitForSeconds(2f);
        x = 0;
    }

    public void Slider()
    {
        for (int i = 0; i < musics.Count; i++)
        {
            musics[i].volume = slide.value;
        }
    }

}
