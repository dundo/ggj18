﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ScrollPitch : MonoBehaviour
{
	public List<AudioSource> theFasterSound;

	void Update ()
	{
		if (Input.GetAxis("Mouse ScrollWheel")>0f ) // forward
		{
			for (int i = 0; i < theFasterSound.Count; i++)
			{
				if (theFasterSound[i].pitch < 3)
				{
					theFasterSound[i].pitch += 0.25f;
				}

			}
		}
		if (Input.GetAxis("Mouse ScrollWheel")<0f) // backwards
		{
			for (int i = 0; i < theFasterSound.Count; i++)
			{
				if (theFasterSound[i].pitch > 0.25f)
				{
					theFasterSound[i].pitch -= 0.25f;
				}

			}
		}
	}
}