﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float keyBoxX;
    public float keyBoxY;

    float keyBoxXL;
    float keyBoxYU;
    float keyBoxXR;
    float keyBoxYD;

    public List<GameObject> doorToOpen;

    public LevelManager levelManager;

    void Start()
    {
        keyBoxXL = transform.position.x - (keyBoxX / 2);
        keyBoxYU = transform.position.y + (keyBoxY / 2);
        keyBoxXR = transform.position.x + (keyBoxX / 2);
        keyBoxYD = transform.position.y - (keyBoxY / 2);

        for (int i = 0; i < doorToOpen.Count; i++)
        {
                doorToOpen[i].SetActive(true);
        }

    }

    void FixedUpdate()
    {
         OpenTheDoor();
    }

    void OnDrawGizmos()
    {
            Gizmos.color = new Color(255, 255, 0, 0.7f);
            Gizmos.DrawCube(transform.position, new Vector2(keyBoxX, keyBoxY));
    }

    void OpenTheDoor()
    {
        if (Controller2D.Instance.transform.position.y <= keyBoxYU)
        {
            if (Controller2D.Instance.transform.position.x >= keyBoxXL)
            {
                if (Controller2D.Instance.transform.position.y >= keyBoxYD)
                {
                    if (Controller2D.Instance.transform.position.x <= keyBoxXR)
                    {
                        for (int i = 0; i < doorToOpen.Count; i++)
                        {
                                doorToOpen[i].SetActive(false);
                        }
                    }
                }
            }
        }
    }
}
