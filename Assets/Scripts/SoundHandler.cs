﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SoundHandler : MonoBehaviour {

    public List<AudioClip> soundClips;
    public List<Button> buttons;
    public List<Toggle> toggles;
    private AudioSource audioSource;
    private Image imageUga;
    public Button confirmButton;
    public string startGame;
    

	void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        imageUga = GetComponent<Image>();
        confirmButton.interactable = false;
        for (int i = 0; i < buttons.Count; i++)
        {
            int j = i;
            buttons[i].onClick.AddListener(() => SelectAudio(j));
            toggles[i].onValueChanged.AddListener((value) => SetAudioClip(value, soundClips[j]));

            var sprite = toggles[i].GetComponent<Image>().sprite;
            toggles[i].onValueChanged.AddListener((value) => SetSprite(value, sprite));
        }
    
    }

    void Update()
    {
        for (int i = 0; i < toggles.Count; i++)
        {
            toggles[i].image.color = Color.white;

            bool inter = SoundManager.instance.savedClips.Count == 5;
            if ( ( inter && !toggles[i].isOn ) )
            {
                toggles[i].interactable = false;
                confirmButton.interactable = true;
                continue;
            }
            confirmButton.interactable = false;
            toggles[i].interactable = true;
            if (toggles[i].isOn)
            {
                toggles[i].image.color = Color.cyan;
            }
            else
            {
          //      toggles[i].image.color = Color.white;
            }
        }
    }

    public void OnConfirm()
    {
        SceneManager.LoadScene(startGame);
    }

    public void SelectAudio(int number)
    {
        audioSource.Stop();
        audioSource.clip = soundClips[number];
        audioSource.Play();
    }

    public void SetAudioClip(bool value, AudioClip audio)
    {
        if (value)
        {
            if (SoundManager.instance.savedClips.Count < 5)
            {
                SoundManager.instance.savedClips.Add(audio);
            }
        }
        else
        {
            SoundManager.instance.savedClips.Remove(audio);
        }

    }

    public void SetSprite(bool value, Sprite sprite)
    {
        if (value)
        {
            if (SoundManager.instance.sprites.Count < 5)
            {
                SoundManager.instance.sprites.Add(sprite);
            }
        }
        else
        {
            SoundManager.instance.sprites.Remove(sprite);
        }

    }
}
