﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 

public class PausaMenu : MonoBehaviour {
	public Canvas canvassina;
	public Canvas canvasConferma;
	public bool pas;
	private int x=0;
	void Start(){
		pas = false;
		canvassina.enabled = false;
		canvasConferma.enabled = false;

	}

	public void CloseGame(){
		//Application.Quit();
		canvassina.enabled = false;
		canvasConferma.enabled = true;
		x = 1;
	}
	public void tornaMenu(){
		//SceneManager.LoadScene ("MainMenu");
		canvassina.enabled = false;
		canvasConferma.enabled = true;
		x = 2;
	}
	public void no(){
		canvassina.enabled = true;
		canvasConferma.enabled = false;
	}
	public void si(){
		if (x == 1) {
			Application.Quit ();
		} else {
			SceneManager.LoadScene ("MainMenu");
		}
	}
	public void res(){
		canvassina.enabled = false;
		pas = false;
	}
	void Update(){
		if ((Input.GetKey (KeyCode.Escape))||(Input.GetKey (KeyCode.P))) {
			 canvassina.enabled = true;
		} 
	}






}
