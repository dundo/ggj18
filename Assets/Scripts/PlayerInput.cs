﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Player))]
public class PlayerInput : MonoBehaviour 
{
    public Controller2D controller;
	void Start () 
	{
        Player.Instance = GetComponent <Player> ();
	}

    void Update() {
        if (controller.collisions.below == true) {
            Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (!Player.Instance.still)
                Player.Instance.SetDirectionalInput(directionalInput);
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button0)) {
            Player.Instance.still = true;
            Player.Instance.GetComponent<Animator>().SetTrigger("jump");
            StartCoroutine(enableMovement(0.3f));
        }

        if (Input.GetKeyUp(KeyCode.Joystick1Button0)) {
            Player.Instance.OnJumpInputUp();
        }
    }


        private IEnumerator enableMovement(float f) {
        yield return new WaitForSeconds(f);
        Player.Instance.still = false;

    }
}
