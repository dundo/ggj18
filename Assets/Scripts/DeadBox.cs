﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBox : MonoBehaviour 
{
	public float deadBoxX;
	public float deadBoxY;

	float deadBoxXL;
	float deadBoxYU;
	float deadBoxXR;
	float deadBoxYD;

	void Start ()
	{
        deadBoxXL = transform.position.x - (deadBoxX / 2);
		deadBoxYU = transform.position.y + (deadBoxY / 2);
		deadBoxXR = transform.position.x + (deadBoxX / 2);
		deadBoxYD = transform.position.y - (deadBoxY / 2);
    }

	void FixedUpdate ()
	{
		Dead ();
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = new Color(0, 0, 0, 0.7f);
		Gizmos.DrawCube(transform.position, new Vector2(deadBoxX, deadBoxY));
	}

    void Dead ()
	{
		if (Controller2D.Instance.transform.position.y <= deadBoxYU) 
		{
			if (Controller2D.Instance.transform.position.x >= deadBoxXL)
			{
				if (Controller2D.Instance.transform.position.y >= deadBoxYD) 
				{
					if (Controller2D.Instance.transform.position.x <= deadBoxXR) 
					{
                        LevelManager.Instance.ResetScene ();
                    }
				}
			}
		}
	}
}
