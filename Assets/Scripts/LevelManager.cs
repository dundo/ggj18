﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public DoorController doorController;

    private void Start()
    {
        Instance = this;
    }

    public void ResetScene ()
	{
        Controller2D.Instance.transform.position = Player.Instance.startPosition;
        ChangeMusicButton.Instance.PlayDeadSound();
        PlayerLife.Instance.LoseALife();

        DoorKeyRespawn();
    }

    public void DoorKeyRespawn ()
    {
        for (int i = 0; i < doorController.doorToOpen.Count; i++)
        {
            if (doorController != null)
                doorController.doorToOpen[i].SetActive(true);
            else
                Debug.LogError("DoorController not set");
        }
    }
}
