﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ToTheNExtLevel : MonoBehaviour
{
    public Image rules;
    public float winBoxX;
    public float winBoxY;

    float winBoxXL;
    float winBoxYU;
    float winBoxXR;
    float winBoxYD;

    void Start()
    {
        rules.gameObject.SetActive(false);

        winBoxXL = transform.position.x - (winBoxX / 2);
        winBoxYU = transform.position.y + (winBoxY / 2);
        winBoxXR = transform.position.x + (winBoxX / 2);
        winBoxYD = transform.position.y - (winBoxY / 2);
    }

    void FixedUpdate()
    {
        Win();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(30, 30, 30, 0.7f);
        Gizmos.DrawCube(transform.position, new Vector2(winBoxX, winBoxY));
    }

    void Win()
    {
        if (Controller2D.Instance.transform.position.y <= winBoxYU)
        {
            if (Controller2D.Instance.transform.position.x >= winBoxXL)
            {
                if (Controller2D.Instance.transform.position.y >= winBoxYD)
                {
                    if (Controller2D.Instance.transform.position.x <= winBoxXR)
                    {
                        rules.gameObject.SetActive(true);
                        ChangeMusicButton.Instance.PlayWinSound();
                        Destroy(gameObject);
                    }
                }
            }
        }
    }
}
