﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceSoundManager : MonoBehaviour
{
    AudioSource savedClipsPlayer;
    public Image rightArrow;
    public Image leftArrow;

    void Start()
    {
        savedClipsPlayer = gameObject.AddComponent<AudioSource>();
        savedClipsPlayer.loop = false;
    }

    private void Update()
    {
        Arrows();
    }

    public void SoundTheSounds(int i)
    {
        savedClipsPlayer.Stop();
        savedClipsPlayer.clip = SoundManager.instance.savedClips[i];
        savedClipsPlayer.Play();
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            savedClipsPlayer.panStereo = -1;

        }
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.RightArrow))
        {
            savedClipsPlayer.panStereo = 1;

        }
        else
        {
            savedClipsPlayer.panStereo = 0;
            rightArrow.GetComponent<Image>().color = new Color(1, 1, 1, 0f);
            leftArrow.GetComponent<Image>().color = new Color(1, 1, 1, 0f);
        }
    }
    public void Arrows()
    {
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            leftArrow.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        }
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.RightArrow))
        {
            rightArrow.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        }
        if (!Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.RightArrow))
        {
            rightArrow.GetComponent<Image>().color = new Color(1, 1, 1, 0f);
        }
        if (!Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            leftArrow.GetComponent<Image>().color = new Color(1, 1, 1, 0f);
        }
    }
}   

