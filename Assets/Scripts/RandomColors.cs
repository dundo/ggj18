﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.PostProcessing;
using UnityEngine;


public class RandomColors : MonoBehaviour
{
    private int passato = -1;
    private int presente;
    private Random rnd;
    public List<PostProcessingProfile> lista;
    private int x = 0;

    void Update()
    {
        if ((Input.GetKey(KeyCode.Joystick1Button3)) && (x == 0))
        {
            x = 1;
            do
            {
                presente = Random.Range(0, lista.Count);
            } while (presente == passato);
            passato = presente;
            GetComponent<PostProcessingBehaviour>().profile = lista[presente];
            StartCoroutine(WaitAndResetX());
        }
    }
    IEnumerator WaitAndResetX()
    {
        yield return new WaitForSeconds(0.5f);
        x = 0;
    }
}

