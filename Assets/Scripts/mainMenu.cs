﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 

public class mainMenu : MonoBehaviour {
	public Image image;
    public string scene;

    public void NewGameBin()
    {
        SceneManager.LoadScene(scene);
    }

	public void CloseGame()
    {
		Application.Quit();
	}
}
