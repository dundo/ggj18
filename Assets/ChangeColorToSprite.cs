﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorToSprite : MonoBehaviour {
    [SerializeField] Color newColor = Color.white;

    public void changeColor()
    {
        GetComponent<SpriteRenderer>().color = newColor;
    }
}
