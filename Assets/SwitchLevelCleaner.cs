﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLevelCleaner : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SoundManager.instance.savedClips.Clear();
        SoundManager.instance.sprites.Clear();
    }
}
